output "cloudwatch_build_log" {
  value = "/aws/codebuild/${aws_codebuild_project.build_app.name}"
}
output "cloudwatch_deploy_log" {
  value = "/aws/codebuild/${aws_codebuild_project.deploy_service.name}"
}
output codepipeline_service_arn {
  value = var.deploy_environment == "prod" ? aws_codepipeline.codepipeline-with-approve[0].arn : aws_codepipeline.codepipeline-without-approve[0].arn
}
output codepipeline_deploy_arn {
  value = aws_codepipeline.codepipeline-deploy.arn
}
