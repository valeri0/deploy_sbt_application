resource "aws_codebuild_project" "build_app" {

  name = "${ var.application_name != "" ? var.application_name: var.repository_name}-build-app"
  build_timeout = "300"
  service_role = var.role_arn_codebuild
  tags = var.tags
  cache {
    type = "S3"
    location = var.s3_cache
  }
  artifacts {
    type = "CODEPIPELINE"
  }
  source {
    type = "CODEPIPELINE"
    buildspec = var.buildspec
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/docker:18.09.0" 
    type                        = "LINUX_CONTAINER"
    privileged_mode = true
    dynamic "environment_variable" {
        for_each = [for v in var.environment_variables: {
          name  = v.name
          value = v.value
        }]

        content {
          name   = environment_variable.value.name
          value  = environment_variable.value.value
        }
      }
  }
}

resource "aws_codebuild_project" "deploy_service" {
  name = "${ var.application_name  != "" ? var.application_name: var.repository_name}-deploy-service"
  build_timeout = "300"
  service_role = var.role_arn_codebuild
  tags = var.tags
  cache {
      type     = "LOCAL"
      modes = ["LOCAL_DOCKER_LAYER_CACHE","LOCAL_CUSTOM_CACHE"]
    }
  artifacts {
    type = "CODEPIPELINE"
  }
  source {
    type = "CODEPIPELINE"
    buildspec = var.deployspec
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/docker:18.09.0" 
    type                        = "LINUX_CONTAINER"
    privileged_mode = true
    dynamic "environment_variable" {
        for_each = [for v in var.environment_variables: {
          name  = v.name
          value = v.value
        }]

        content {
          name   = environment_variable.value.name
          value  = environment_variable.value.value
        }
      }
  }
}
#until https://github.com/terraform-providers/terraform-provider-aws/issues/924 is done need to duplicate code

resource "aws_codepipeline" "codepipeline-with-approve" {
  count = var.deploy_environment == "prod" ? 1 : 0
  name =  var.application_name != "" ? var.application_name: var.repository_name
  depends_on = [var.vm_depends_on]
  role_arn = var.role_arn_codepipeline

  artifact_store {
    location = var.codepipeline_bucket
    type     = "S3"
  }
  stage {
    name = "Source"
    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        PollForSourceChanges = var.poll_for_source_changes
        RepositoryName   = var.repository_name
        BranchName = var.branch_name
      }
    }
  }
  stage {
    name = "build_app"

    action {
      name     = "Build"
      category = "Build"
      owner    = "AWS"
      provider = "CodeBuild"
      version  = "1"
      input_artifacts = ["source_output"]
      output_artifacts = ["build_output"]

      configuration = {
        ProjectName = aws_codebuild_project.build_app.name
      }
    }
  }
stage {
  name = "Approve"

  action {
    name     = "Approval"
    category = "Approval"
    owner    = "AWS"
    provider = "Manual"
    version  = "1"

    configuration = {
      CustomData = "There is need for manual approval"
    }
  }
}
  stage {
    name = "deploy_service"

    action {
      name     = "Build"
      category = "Build"
      owner    = "AWS"
      provider = "CodeBuild"
      version  = "1"
      input_artifacts = ["build_output"]


      configuration = {
        ProjectName = aws_codebuild_project.deploy_service.name
      }
    }
  }
  tags = var.tags
}
resource "aws_codepipeline" "codepipeline-deploy" {
  name     = "${var.repository_name}-deploy"
  role_arn = var.role_arn_codepipeline

  artifact_store {
    location = var.codepipeline_bucket
    type     = "S3"
    # TODO remember to parametrize
    encryption_key {
      id = "arn:aws:kms:eu-west-1:796341525871:key/e9141a5d-f993-464d-af9e-82f5272c85f9"
      type = "KMS"
    }
  }
  stage {
    name = "Source"
    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["source_output"]
      role_arn         = var.role_arn_source
      configuration = {
        PollForSourceChanges = var.poll_for_source_changes
        RepositoryName   = "${var.repository_name}-deploy"
        BranchName = var.branch_name
      }
    }
  }
  stage {
    name = "Deploy-app"
    action {
      name     = "Deploy"
      category = "Deploy"
      owner    = "AWS"
      provider = "ECS"
      version  = "1"
      input_artifacts = ["source_output"]


      configuration = {
        ClusterName= "fdh-cluster-dev"
        ServiceName= var.repository_name
      }
    }
  }
  tags = var.tags
}
resource "aws_codepipeline" "codepipeline-without-approve" {
  count = var.deploy_environment == "test" ? 1 : 0
  name =  var.application_name != "" ? var.application_name: var.repository_name
  role_arn = var.role_arn_codepipeline

  artifact_store {
    location = var.codepipeline_bucket
    type     = "S3"
    # TODO remember to parametrize
    encryption_key {
      id = "arn:aws:kms:eu-west-1:796341525871:key/e9141a5d-f993-464d-af9e-82f5272c85f9"
      type = "KMS"
    }
  }
  stage {
    name = "Source"
    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["source_output"]
      role_arn         = var.role_arn_source
      configuration = {
        PollForSourceChanges = var.poll_for_source_changes
        RepositoryName   = var.repository_name
        BranchName = var.branch_name
      }
    }
  }
  stage {
    name = "build_app"

    action {
      name     = "Build"
      category = "Build"
      owner    = "AWS"
      provider = "CodeBuild"
      version  = "1"
      input_artifacts = ["source_output"]
      output_artifacts = ["build_output"]

      configuration = {
        ProjectName = aws_codebuild_project.build_app.name
      }
    }
  }

  stage {
    name = "deploy_service"

    action {
      name     = "Build"
      category = "Build"
      owner    = "AWS"
      provider = "CodeBuild"
      version  = "1"
      input_artifacts = ["build_output"]

      configuration = {
        ProjectName = aws_codebuild_project.deploy_service.name
      }
    }
  }
  tags = var.tags
}
