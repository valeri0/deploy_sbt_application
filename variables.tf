variable "branch_name" {
  default = "master"
}
variable "buildspec" {}
variable "application_name" {
  default = ""
}
variable "codepipeline_bucket" {}
variable "create_ecr_repository" {
  default = true
}
variable "vm_depends_on" {
  type    = any
  default = null
}
variable "deploy_environment" {}
variable "deployspec" {}
variable "environment_variables" {
    type = list
    default = []

}
variable "poll_for_source_changes" { default  = "true" }
variable "repository_name" {}
variable "role_arn" {}
variable "role_arn_codebuild" {}
variable "role_arn_codepipeline" {}
variable "role_arn_source" {}
variable "s3_cache" {}
variable "tags" {}
